#!/usr/bin/env bash
#
# Test SSH Key upload with curl

APPPASSWORD=XXXXX
bitbucket_password=XXXXX
USERNAME=robomfeinberg
bitbucket_username=robomfeinberg
USERKEY=$(cat /tmp/testkey.pem.pub)

#echo $USERKEY

curl -u "$USERNAME:$APPPASSWORD" https://bitbucket.org/api/2.0/users/$USERNAME/ssh-keys --data-urlencode "key=$(cat /tmp/testkey.pem.pub)" --data-urlencode "label=AAAtest-key-uploader"

test_creds () {

    # Validate Bitbucket credentials
    echo "Validating Bitbucket credentials...";
    httpStatus=$(curl -s -o /dev/null -I -w "%{http_code}"  --user ${bitbucket_username}:${bitbucket_password} https://api.bitbucket.org/2.0/user);
    if [[ 200 == $httpStatus ]]; then
        bitbucketCredentialsValid=true;
    else
        tput setaf ${color[red]};
        echo "Bitbucket credentials failed. Please re-enter.";
        tput setaf ${color[default]};
        unset bitbucket_username
        unset bitbucket_password
    fi

}

run_upload () {

echo "Testing Key Upload"
echo "============================"
php  uploadSshKey.php -umfeinberg@robofirm.com -a$bitbucket_username -kAAAmfeinberguploadtest -p$bitbucket_password

}

# functions begen here
#test_creds
#run_upload