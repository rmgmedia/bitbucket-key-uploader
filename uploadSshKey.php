<?php
include "vendor/autoload.php";

/*
* This uploader uses the following php library: http://gentlero.bitbucket.org/bitbucket-api/
* You will need to run a composer install for this to work correctly.
* It requires the following arguments to be present to work:
* -uemail@robofirm.com (required) # This is the authentication email
* -ppasswordhere (required)       # This is the authentication password
* -aaccountname (optional)        # Bitbucket account to attach ssh key to
* -kkeylabelnamehere (required)   # The key label name 
*/

// First, check and grab all the arguments from command line
$options = getopt("u:p:a:k:");
if(!array_key_exists("u",$options)){
    echo "ERROR: Requires Bitbucket User. (-uemail@robofirm.com)\n";
    exit(1);
}else{
    $bb_user = $options['u'];
}
if(!array_key_exists("p",$options)){
    echo "ERROR: Requires Bitbucket Password. (-p passwordhere)\n";
    exit(1);
}else{
    $bb_pass = $options['p'];
}
if(!array_key_exists("a",$options)){
    $account_name = $bb_user;
}else{
    $account_name = $options['a'];
}
if(!array_key_exists("k",$options)){
    echo "ERROR: Requires Bitbucket SSH Key Label Name. (-k staging-hostname.robofirm.net)\n";
    exit(1);

}else{
    $key_name = $options['k'];
}

// Grab the ssh pub key
//$ssh_key_file_public = "/tmp/testkey.pem.pub";
$ssh_key_file_public = $_SERVER['HOME']."/.ssh/id_rsa.pub";
if(!file_exists($ssh_key_file_public)){
    echo "ERROR: id_rsa.pub not found.\n";
    exit(1);
}
$key_contents = trim(file_get_contents($ssh_key_file_public));


// Output a little text about what was sent
echo "Adding ssh key to bitbucket:\n";
echo " - bitbucket auth: ".$bb_user."\n";
echo " - account name: ".$account_name."\n";
echo " - key name: ".$key_name."\n";
echo " - key contents: ".$key_contents."\n";

// Updated to latest api code
// Setup Authentication
$users = new Bitbucket\API\Users();
$users->getClient()->addListener(
    new Bitbucket\API\Http\Listener\BasicAuthListener($account_name, $bb_pass)
);

// Grab a list of keys from the user - used below to check if key exists
$bitbucket_key_list = $users->sshKeys()->all($account_name);
$bitbucketapi_headers = $bitbucket_key_list->getHeaders();
$bitbucket_key_list_content = $bitbucket_key_list->getContent();

// Error check - Authentication
if($bitbucket_key_list_content == ""){
    echo "ERROR: Could not Authenticate or other issues with bitbucket\n";
    echo "bitbucket api headers: \n";
    print_r($bitbucketapi_headers);
    exit(1);
}

// Error check - User doesnt exist
if(substr($bitbucket_key_list_content,0,7) == "No user"){
    echo "ERROR: ".$bitbucket_key_list_content."\n";
    exit(1);
}


// Grab a list of keys from the user - used below to check if key exists has to fetch all pages
$page = new \Bitbucket\API\Http\Response\Pager($users->getClient(),$users->sshKeys()->all($account_name));
$response = $page->fetchAll();

$bitbucketapi_headers = $bitbucket_key_list->getHeaders();
$bitbucket_key_list_content = $bitbucket_key_list->getContent();

// Error check - Authentication
if($bitbucket_key_list_content == ""){
    echo "ERROR: Could not Authenticate or other issues with bitbucket\n";
    echo "bitbucket api headers: \n";
    print_r($bitbucketapi_headers);
    exit(1);
}

// Error check - User doesnt exist
if(substr($bitbucket_key_list_content,0,7) == "No user"){
    echo "ERROR: ".$bitbucket_key_list_content."\n";
    //echo "There is no key match";
    //exit(1);
}

// Check to see if key exists - done by name and key value
$bitbucket_key_list = json_decode($bitbucket_key_list_content,true);


foreach($bitbucket_key_list['values'] as $key=>$value){
    $sshkey =  $value['key'] . ' ' . $value['comment'];
    //echo $value['label']."=".$value['key'] . "\n" ;
    if($sshkey == $key_contents) {
        //if($value['key'] == $key_contents) {
        echo "The key matches!!!!. We will attempt to delete the key\n";
    }
    // This is redundant
//    elseif (!$sshkey) {
//         echo "There is no key match!!!!.\n";
//        }
}


// Get UUID
foreach($bitbucket_key_list['values'] as $key=>$value) {
    $sshkey = $value['key'] . ' ' . $value['comment'];
    $uuid = $value['uuid'];
    //echo "This is the UUID \n";
    //print_r($uuid);
    //echo "  \n";
    if ($sshkey == $key_contents) {

        echo "begin curling to delete the conflicting key\n";

// the gentle/bitbucket-api does not work with the new api 2.0 and app passwords
// Switching here to plain GuzzleHttp and doing direct API calls. We can probably switch the above as well
        $client = new GuzzleHttp\Client();
        $response = $client->request(
            'DELETE',
            "https://api.bitbucket.org/2.0/users/$account_name/ssh-keys/$uuid",
            [
                "auth" => ["$account_name", "$bb_pass"]
            ]
        );

        echo $response->getBody();

        echo "Found and removed existing key based on key match.\n";
        echo "UUID: $uuid \n";
    } elseif ($value['label'] == $key_name) {

        // This is ever actually happen?

        $client = new GuzzleHttp\Client();
        $response = $client->request(
            'DELETE',
            "https://api.bitbucket.org/2.0/users/$account_name/ssh-keys/$uuid",
            [
                "auth" => ["$account_name", "$bb_pass"]
            ]
        );

        echo $response->getBody();

        echo "Found and removed existing key based on key name.\n";
        echo "UUID: $uuid \n";

    }
}

// Add the ssh key to the account
echo "Attempting to add new key to bit bucket";
$client = new GuzzleHttp\Client();
$response = $client->request(
    'POST',
    "https://api.bitbucket.org/2.0/users/$account_name/ssh-keys",
    [
        "auth" => ["$account_name", "$bb_pass"],
        "json" => [
            "label" => $key_name,
            "key" => $key_contents
        ]
    ]
);


$key_upload = json_decode($response->getBody());
print_r($key_upload);


// We can not easily check the key content. If there is no UUID in the reply then it did not work
if($key_upload != "" && $key_upload->uuid != ""){
    echo "SUCCESS: Added key to bitbucket.\n";

    $bitbucketlockfile = $_SERVER['HOME']."/.bitbucketssh";
    echo "Adding lock file at: $bitbucketlockfile \n";
    touch($bitbucketlockfile);

    exit(0);
}else{
    echo "FAILED TO SEND KEY\n";
    echo "Response from bitbucket API:\n";
    print_r($response->getBody());
    echo "\n";
    exit(1);
}
